const promise1 = new Promise((resolve, reject) => {
  const fs = require("fs");
  fs.readFile("file1.txt", "utf8", function (err, data) {
    if (err) {
      reject(err);
    } else {
      const numsStr = data.replace(/[^0-9]/g, ",");
      const str = numsStr
        .split(",")
        .filter((data) => data)
        .map((i) => Number(i));

      const addNumber = str.reduce((sum, number) => sum + number);
      resolve(addNumber);
      console.log(str);
    }
  });
});
const promise2 = new Promise((resolve, reject) => {
  const fs = require("fs");
  fs.readFile("file2.txt", "utf8", function (err, data) {
    if (err) {
      reject(err);
    } else {
      const numsStr = data.replace(/[^0-9]/g, ",");
      const str = numsStr
        .split(",")
        .filter((data) => data)
        .map((i) => Number(i));

      const addNumber = str.reduce((sum, number) => sum + number);
      resolve(addNumber);

      console.log(str);
    }
  });
});
Promise.all([promise1, promise2])
  .then((addNumber) => {
    console.log(addNumber);
  })
  .catch((err) => {
    console.log(err);
  });
