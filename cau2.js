const { rejects } = require("assert");
const { resolve } = require("path");

const promise = new Promise((resolve, rejects) => {
  const fs = require("fs");
  fs.readFile("file2.txt", "utf8", (err, data) => {
    if (err) {
      console.log("Error reading file ");
      reject(err);
    } else {
      if (data.indexOf("mutual") >= 0) {
        const existString = "Chuỗi hợp lệ";
        resolve(existString);
      } else {
        const existStringFail = "chuỗi không hợp lệ";
        rejects(existStringFail);
      }
    }
  });
});

promise
  .then((ok) => {
    console.log(ok);
  })
  .catch((err) => {
    console.log(err);
  });
