function readContent(callback) {
  const fs = require("fs");
  fs.readFile("file1.txt", "utf8", function (err, content) {
    if (err) return callback(err);
    callback(null, content);
  });
}

readContent(function (err, content) {
  console.log(content);
});
