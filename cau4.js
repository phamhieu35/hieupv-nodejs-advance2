let fs = require("fs");
let fname = "file1.txt";
let fname2 = "file2.txt";
async function readFile(filename) {
  try {
    let data = await fs.promises.readFile(filename, "utf8");

    console.log(data.split("in").length - 1);
  } catch (err) {
    console.log(err);
  }
}
readFile(fname);
readFile(fname2);
